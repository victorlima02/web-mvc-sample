MVC Sample Test
======

This is an enterprise MVC web application sample: log related entities.

Requires:
- Java SE 11.

## Help
To install the server for this project, use:

```
./gradlew initBundle
```

To build this project, use:

```
./gradlew buildService build
```

To deploy this project, use:

```
./gradlew deploy
```

To run the main example, use:

```
./bundles/tomcat-9.0.37/bin/startup.sh 
```

To add this portlet compliant application to the main page:

- Click on the top right conner pencil
- On the right menu, select widget: Sample category > Access

