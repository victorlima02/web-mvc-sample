/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package question.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class MachineSoap implements Serializable {

	public static MachineSoap toSoapModel(Machine model) {
		MachineSoap soapModel = new MachineSoap();

		soapModel.setMachineId(model.getMachineId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setAdminId(model.getAdminId());

		return soapModel;
	}

	public static MachineSoap[] toSoapModels(Machine[] models) {
		MachineSoap[] soapModels = new MachineSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MachineSoap[][] toSoapModels(Machine[][] models) {
		MachineSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MachineSoap[models.length][models[0].length];
		}
		else {
			soapModels = new MachineSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MachineSoap[] toSoapModels(List<Machine> models) {
		List<MachineSoap> soapModels = new ArrayList<MachineSoap>(
			models.size());

		for (Machine model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MachineSoap[soapModels.size()]);
	}

	public MachineSoap() {
	}

	public long getPrimaryKey() {
		return _machineId;
	}

	public void setPrimaryKey(long pk) {
		setMachineId(pk);
	}

	public long getMachineId() {
		return _machineId;
	}

	public void setMachineId(long machineId) {
		_machineId = machineId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getAdminId() {
		return _adminId;
	}

	public void setAdminId(long adminId) {
		_adminId = adminId;
	}

	private long _machineId;
	private long _companyId;
	private long _groupId;
	private long _adminId;

}