/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package question.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Access}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Access
 * @generated
 */
public class AccessWrapper
	extends BaseModelWrapper<Access> implements Access, ModelWrapper<Access> {

	public AccessWrapper(Access access) {
		super(access);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("accessId", getAccessId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userName", getUserName());
		attributes.put("accessDate", getAccessDate());
		attributes.put("machineId", getMachineId());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long accessId = (Long)attributes.get("accessId");

		if (accessId != null) {
			setAccessId(accessId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date accessDate = (Date)attributes.get("accessDate");

		if (accessDate != null) {
			setAccessDate(accessDate);
		}

		Long machineId = (Long)attributes.get("machineId");

		if (machineId != null) {
			setMachineId(machineId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	/**
	 * Returns the access date of this access.
	 *
	 * @return the access date of this access
	 */
	@Override
	public Date getAccessDate() {
		return model.getAccessDate();
	}

	/**
	 * Returns the access ID of this access.
	 *
	 * @return the access ID of this access
	 */
	@Override
	public long getAccessId() {
		return model.getAccessId();
	}

	/**
	 * Returns the company ID of this access.
	 *
	 * @return the company ID of this access
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the group ID of this access.
	 *
	 * @return the group ID of this access
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the machine ID of this access.
	 *
	 * @return the machine ID of this access
	 */
	@Override
	public long getMachineId() {
		return model.getMachineId();
	}

	/**
	 * Returns the primary key of this access.
	 *
	 * @return the primary key of this access
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this access.
	 *
	 * @return the user ID of this access
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this access.
	 *
	 * @return the user name of this access
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this access.
	 *
	 * @return the user uuid of this access
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this access.
	 *
	 * @return the uuid of this access
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the access date of this access.
	 *
	 * @param accessDate the access date of this access
	 */
	@Override
	public void setAccessDate(Date accessDate) {
		model.setAccessDate(accessDate);
	}

	/**
	 * Sets the access ID of this access.
	 *
	 * @param accessId the access ID of this access
	 */
	@Override
	public void setAccessId(long accessId) {
		model.setAccessId(accessId);
	}

	/**
	 * Sets the company ID of this access.
	 *
	 * @param companyId the company ID of this access
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the group ID of this access.
	 *
	 * @param groupId the group ID of this access
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the machine ID of this access.
	 *
	 * @param machineId the machine ID of this access
	 */
	@Override
	public void setMachineId(long machineId) {
		model.setMachineId(machineId);
	}

	/**
	 * Sets the primary key of this access.
	 *
	 * @param primaryKey the primary key of this access
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this access.
	 *
	 * @param userId the user ID of this access
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this access.
	 *
	 * @param userName the user name of this access
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this access.
	 *
	 * @param userUuid the user uuid of this access
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this access.
	 *
	 * @param uuid the uuid of this access
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	protected AccessWrapper wrap(Access access) {
		return new AccessWrapper(access);
	}

}