/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package question.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class AccessSoap implements Serializable {

	public static AccessSoap toSoapModel(Access model) {
		AccessSoap soapModel = new AccessSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setAccessId(model.getAccessId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserName(model.getUserName());
		soapModel.setAccessDate(model.getAccessDate());
		soapModel.setMachineId(model.getMachineId());
		soapModel.setUserId(model.getUserId());

		return soapModel;
	}

	public static AccessSoap[] toSoapModels(Access[] models) {
		AccessSoap[] soapModels = new AccessSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AccessSoap[][] toSoapModels(Access[][] models) {
		AccessSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AccessSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AccessSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AccessSoap[] toSoapModels(List<Access> models) {
		List<AccessSoap> soapModels = new ArrayList<AccessSoap>(models.size());

		for (Access model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AccessSoap[soapModels.size()]);
	}

	public AccessSoap() {
	}

	public long getPrimaryKey() {
		return _accessId;
	}

	public void setPrimaryKey(long pk) {
		setAccessId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getAccessId() {
		return _accessId;
	}

	public void setAccessId(long accessId) {
		_accessId = accessId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getAccessDate() {
		return _accessDate;
	}

	public void setAccessDate(Date accessDate) {
		_accessDate = accessDate;
	}

	public long getMachineId() {
		return _machineId;
	}

	public void setMachineId(long machineId) {
		_machineId = machineId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	private String _uuid;
	private long _accessId;
	private long _companyId;
	private long _groupId;
	private String _userName;
	private Date _accessDate;
	private long _machineId;
	private long _userId;

}