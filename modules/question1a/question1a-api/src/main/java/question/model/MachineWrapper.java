/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package question.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Machine}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Machine
 * @generated
 */
public class MachineWrapper
	extends BaseModelWrapper<Machine>
	implements Machine, ModelWrapper<Machine> {

	public MachineWrapper(Machine machine) {
		super(machine);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("machineId", getMachineId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("adminId", getAdminId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long machineId = (Long)attributes.get("machineId");

		if (machineId != null) {
			setMachineId(machineId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long adminId = (Long)attributes.get("adminId");

		if (adminId != null) {
			setAdminId(adminId);
		}
	}

	/**
	 * Returns the admin ID of this machine.
	 *
	 * @return the admin ID of this machine
	 */
	@Override
	public long getAdminId() {
		return model.getAdminId();
	}

	/**
	 * Returns the company ID of this machine.
	 *
	 * @return the company ID of this machine
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the group ID of this machine.
	 *
	 * @return the group ID of this machine
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the machine ID of this machine.
	 *
	 * @return the machine ID of this machine
	 */
	@Override
	public long getMachineId() {
		return model.getMachineId();
	}

	/**
	 * Returns the primary key of this machine.
	 *
	 * @return the primary key of this machine
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the admin ID of this machine.
	 *
	 * @param adminId the admin ID of this machine
	 */
	@Override
	public void setAdminId(long adminId) {
		model.setAdminId(adminId);
	}

	/**
	 * Sets the company ID of this machine.
	 *
	 * @param companyId the company ID of this machine
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the group ID of this machine.
	 *
	 * @param groupId the group ID of this machine
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the machine ID of this machine.
	 *
	 * @param machineId the machine ID of this machine
	 */
	@Override
	public void setMachineId(long machineId) {
		model.setMachineId(machineId);
	}

	/**
	 * Sets the primary key of this machine.
	 *
	 * @param primaryKey the primary key of this machine
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	@Override
	protected MachineWrapper wrap(Machine machine) {
		return new MachineWrapper(machine);
	}

}