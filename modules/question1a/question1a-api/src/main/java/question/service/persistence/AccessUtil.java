/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package question.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

import question.model.Access;

/**
 * The persistence utility for the access service. This utility wraps <code>question.service.persistence.impl.AccessPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AccessPersistence
 * @generated
 */
public class AccessUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Access access) {
		getPersistence().clearCache(access);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Access> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Access> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Access> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Access> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Access> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Access update(Access access) {
		return getPersistence().update(access);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Access update(Access access, ServiceContext serviceContext) {
		return getPersistence().update(access, serviceContext);
	}

	/**
	 * Returns all the accesses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching accesses
	 */
	public static List<Access> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the accesses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @return the range of matching accesses
	 */
	public static List<Access> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the accesses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Access> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the accesses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Access> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first access in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findByUuid_First(
			String uuid, OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first access in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchByUuid_First(
		String uuid, OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findByUuid_Last(
			String uuid, OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchByUuid_Last(
		String uuid, OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the accesses before and after the current access in the ordered set where uuid = &#63;.
	 *
	 * @param accessId the primary key of the current access
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next access
	 * @throws NoSuchAccessException if a access with the primary key could not be found
	 */
	public static Access[] findByUuid_PrevAndNext(
			long accessId, String uuid,
			OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByUuid_PrevAndNext(
			accessId, uuid, orderByComparator);
	}

	/**
	 * Removes all the accesses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of accesses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching accesses
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the access where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchAccessException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findByUUID_G(String uuid, long groupId)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the access where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the access where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the access where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the access that was removed
	 */
	public static Access removeByUUID_G(String uuid, long groupId)
		throws question.exception.NoSuchAccessException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of accesses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching accesses
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the accesses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching accesses
	 */
	public static List<Access> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the accesses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @return the range of matching accesses
	 */
	public static List<Access> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the accesses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Access> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the accesses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Access> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first access in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first access in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the accesses before and after the current access in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param accessId the primary key of the current access
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next access
	 * @throws NoSuchAccessException if a access with the primary key could not be found
	 */
	public static Access[] findByUuid_C_PrevAndNext(
			long accessId, String uuid, long companyId,
			OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByUuid_C_PrevAndNext(
			accessId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the accesses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of accesses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching accesses
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the accesses where machineId = &#63;.
	 *
	 * @param machineId the machine ID
	 * @return the matching accesses
	 */
	public static List<Access> findBymachine(long machineId) {
		return getPersistence().findBymachine(machineId);
	}

	/**
	 * Returns a range of all the accesses where machineId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param machineId the machine ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @return the range of matching accesses
	 */
	public static List<Access> findBymachine(
		long machineId, int start, int end) {

		return getPersistence().findBymachine(machineId, start, end);
	}

	/**
	 * Returns an ordered range of all the accesses where machineId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param machineId the machine ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findBymachine(
		long machineId, int start, int end,
		OrderByComparator<Access> orderByComparator) {

		return getPersistence().findBymachine(
			machineId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the accesses where machineId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param machineId the machine ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findBymachine(
		long machineId, int start, int end,
		OrderByComparator<Access> orderByComparator, boolean useFinderCache) {

		return getPersistence().findBymachine(
			machineId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first access in the ordered set where machineId = &#63;.
	 *
	 * @param machineId the machine ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findBymachine_First(
			long machineId, OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findBymachine_First(
			machineId, orderByComparator);
	}

	/**
	 * Returns the first access in the ordered set where machineId = &#63;.
	 *
	 * @param machineId the machine ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchBymachine_First(
		long machineId, OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchBymachine_First(
			machineId, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where machineId = &#63;.
	 *
	 * @param machineId the machine ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findBymachine_Last(
			long machineId, OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findBymachine_Last(
			machineId, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where machineId = &#63;.
	 *
	 * @param machineId the machine ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchBymachine_Last(
		long machineId, OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchBymachine_Last(
			machineId, orderByComparator);
	}

	/**
	 * Returns the accesses before and after the current access in the ordered set where machineId = &#63;.
	 *
	 * @param accessId the primary key of the current access
	 * @param machineId the machine ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next access
	 * @throws NoSuchAccessException if a access with the primary key could not be found
	 */
	public static Access[] findBymachine_PrevAndNext(
			long accessId, long machineId,
			OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findBymachine_PrevAndNext(
			accessId, machineId, orderByComparator);
	}

	/**
	 * Removes all the accesses where machineId = &#63; from the database.
	 *
	 * @param machineId the machine ID
	 */
	public static void removeBymachine(long machineId) {
		getPersistence().removeBymachine(machineId);
	}

	/**
	 * Returns the number of accesses where machineId = &#63;.
	 *
	 * @param machineId the machine ID
	 * @return the number of matching accesses
	 */
	public static int countBymachine(long machineId) {
		return getPersistence().countBymachine(machineId);
	}

	/**
	 * Returns all the accesses where accessDate = &#63;.
	 *
	 * @param accessDate the access date
	 * @return the matching accesses
	 */
	public static List<Access> findBydate(Date accessDate) {
		return getPersistence().findBydate(accessDate);
	}

	/**
	 * Returns a range of all the accesses where accessDate = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param accessDate the access date
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @return the range of matching accesses
	 */
	public static List<Access> findBydate(Date accessDate, int start, int end) {
		return getPersistence().findBydate(accessDate, start, end);
	}

	/**
	 * Returns an ordered range of all the accesses where accessDate = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param accessDate the access date
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findBydate(
		Date accessDate, int start, int end,
		OrderByComparator<Access> orderByComparator) {

		return getPersistence().findBydate(
			accessDate, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the accesses where accessDate = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param accessDate the access date
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findBydate(
		Date accessDate, int start, int end,
		OrderByComparator<Access> orderByComparator, boolean useFinderCache) {

		return getPersistence().findBydate(
			accessDate, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first access in the ordered set where accessDate = &#63;.
	 *
	 * @param accessDate the access date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findBydate_First(
			Date accessDate, OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findBydate_First(accessDate, orderByComparator);
	}

	/**
	 * Returns the first access in the ordered set where accessDate = &#63;.
	 *
	 * @param accessDate the access date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchBydate_First(
		Date accessDate, OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchBydate_First(
			accessDate, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where accessDate = &#63;.
	 *
	 * @param accessDate the access date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findBydate_Last(
			Date accessDate, OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findBydate_Last(accessDate, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where accessDate = &#63;.
	 *
	 * @param accessDate the access date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchBydate_Last(
		Date accessDate, OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchBydate_Last(accessDate, orderByComparator);
	}

	/**
	 * Returns the accesses before and after the current access in the ordered set where accessDate = &#63;.
	 *
	 * @param accessId the primary key of the current access
	 * @param accessDate the access date
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next access
	 * @throws NoSuchAccessException if a access with the primary key could not be found
	 */
	public static Access[] findBydate_PrevAndNext(
			long accessId, Date accessDate,
			OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findBydate_PrevAndNext(
			accessId, accessDate, orderByComparator);
	}

	/**
	 * Removes all the accesses where accessDate = &#63; from the database.
	 *
	 * @param accessDate the access date
	 */
	public static void removeBydate(Date accessDate) {
		getPersistence().removeBydate(accessDate);
	}

	/**
	 * Returns the number of accesses where accessDate = &#63;.
	 *
	 * @param accessDate the access date
	 * @return the number of matching accesses
	 */
	public static int countBydate(Date accessDate) {
		return getPersistence().countBydate(accessDate);
	}

	/**
	 * Returns all the accesses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching accesses
	 */
	public static List<Access> findByuser(long userId) {
		return getPersistence().findByuser(userId);
	}

	/**
	 * Returns a range of all the accesses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @return the range of matching accesses
	 */
	public static List<Access> findByuser(long userId, int start, int end) {
		return getPersistence().findByuser(userId, start, end);
	}

	/**
	 * Returns an ordered range of all the accesses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findByuser(
		long userId, int start, int end,
		OrderByComparator<Access> orderByComparator) {

		return getPersistence().findByuser(
			userId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the accesses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findByuser(
		long userId, int start, int end,
		OrderByComparator<Access> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByuser(
			userId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first access in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findByuser_First(
			long userId, OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByuser_First(userId, orderByComparator);
	}

	/**
	 * Returns the first access in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchByuser_First(
		long userId, OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchByuser_First(userId, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findByuser_Last(
			long userId, OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByuser_Last(userId, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchByuser_Last(
		long userId, OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchByuser_Last(userId, orderByComparator);
	}

	/**
	 * Returns the accesses before and after the current access in the ordered set where userId = &#63;.
	 *
	 * @param accessId the primary key of the current access
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next access
	 * @throws NoSuchAccessException if a access with the primary key could not be found
	 */
	public static Access[] findByuser_PrevAndNext(
			long accessId, long userId,
			OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByuser_PrevAndNext(
			accessId, userId, orderByComparator);
	}

	/**
	 * Removes all the accesses where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public static void removeByuser(long userId) {
		getPersistence().removeByuser(userId);
	}

	/**
	 * Returns the number of accesses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching accesses
	 */
	public static int countByuser(long userId) {
		return getPersistence().countByuser(userId);
	}

	/**
	 * Returns all the accesses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching accesses
	 */
	public static List<Access> findByCompanyId(long companyId) {
		return getPersistence().findByCompanyId(companyId);
	}

	/**
	 * Returns a range of all the accesses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @return the range of matching accesses
	 */
	public static List<Access> findByCompanyId(
		long companyId, int start, int end) {

		return getPersistence().findByCompanyId(companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the accesses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findByCompanyId(
		long companyId, int start, int end,
		OrderByComparator<Access> orderByComparator) {

		return getPersistence().findByCompanyId(
			companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the accesses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching accesses
	 */
	public static List<Access> findByCompanyId(
		long companyId, int start, int end,
		OrderByComparator<Access> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByCompanyId(
			companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first access in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findByCompanyId_First(
			long companyId, OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByCompanyId_First(
			companyId, orderByComparator);
	}

	/**
	 * Returns the first access in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchByCompanyId_First(
		long companyId, OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchByCompanyId_First(
			companyId, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access
	 * @throws NoSuchAccessException if a matching access could not be found
	 */
	public static Access findByCompanyId_Last(
			long companyId, OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByCompanyId_Last(
			companyId, orderByComparator);
	}

	/**
	 * Returns the last access in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching access, or <code>null</code> if a matching access could not be found
	 */
	public static Access fetchByCompanyId_Last(
		long companyId, OrderByComparator<Access> orderByComparator) {

		return getPersistence().fetchByCompanyId_Last(
			companyId, orderByComparator);
	}

	/**
	 * Returns the accesses before and after the current access in the ordered set where companyId = &#63;.
	 *
	 * @param accessId the primary key of the current access
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next access
	 * @throws NoSuchAccessException if a access with the primary key could not be found
	 */
	public static Access[] findByCompanyId_PrevAndNext(
			long accessId, long companyId,
			OrderByComparator<Access> orderByComparator)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByCompanyId_PrevAndNext(
			accessId, companyId, orderByComparator);
	}

	/**
	 * Removes all the accesses where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	public static void removeByCompanyId(long companyId) {
		getPersistence().removeByCompanyId(companyId);
	}

	/**
	 * Returns the number of accesses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching accesses
	 */
	public static int countByCompanyId(long companyId) {
		return getPersistence().countByCompanyId(companyId);
	}

	/**
	 * Caches the access in the entity cache if it is enabled.
	 *
	 * @param access the access
	 */
	public static void cacheResult(Access access) {
		getPersistence().cacheResult(access);
	}

	/**
	 * Caches the accesses in the entity cache if it is enabled.
	 *
	 * @param accesses the accesses
	 */
	public static void cacheResult(List<Access> accesses) {
		getPersistence().cacheResult(accesses);
	}

	/**
	 * Creates a new access with the primary key. Does not add the access to the database.
	 *
	 * @param accessId the primary key for the new access
	 * @return the new access
	 */
	public static Access create(long accessId) {
		return getPersistence().create(accessId);
	}

	/**
	 * Removes the access with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param accessId the primary key of the access
	 * @return the access that was removed
	 * @throws NoSuchAccessException if a access with the primary key could not be found
	 */
	public static Access remove(long accessId)
		throws question.exception.NoSuchAccessException {

		return getPersistence().remove(accessId);
	}

	public static Access updateImpl(Access access) {
		return getPersistence().updateImpl(access);
	}

	/**
	 * Returns the access with the primary key or throws a <code>NoSuchAccessException</code> if it could not be found.
	 *
	 * @param accessId the primary key of the access
	 * @return the access
	 * @throws NoSuchAccessException if a access with the primary key could not be found
	 */
	public static Access findByPrimaryKey(long accessId)
		throws question.exception.NoSuchAccessException {

		return getPersistence().findByPrimaryKey(accessId);
	}

	/**
	 * Returns the access with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param accessId the primary key of the access
	 * @return the access, or <code>null</code> if a access with the primary key could not be found
	 */
	public static Access fetchByPrimaryKey(long accessId) {
		return getPersistence().fetchByPrimaryKey(accessId);
	}

	/**
	 * Returns all the accesses.
	 *
	 * @return the accesses
	 */
	public static List<Access> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the accesses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @return the range of accesses
	 */
	public static List<Access> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the accesses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of accesses
	 */
	public static List<Access> findAll(
		int start, int end, OrderByComparator<Access> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the accesses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AccessModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of accesses
	 * @param end the upper bound of the range of accesses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of accesses
	 */
	public static List<Access> findAll(
		int start, int end, OrderByComparator<Access> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the accesses from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of accesses.
	 *
	 * @return the number of accesses
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static AccessPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AccessPersistence, AccessPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(AccessPersistence.class);

		ServiceTracker<AccessPersistence, AccessPersistence> serviceTracker =
			new ServiceTracker<AccessPersistence, AccessPersistence>(
				bundle.getBundleContext(), AccessPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}