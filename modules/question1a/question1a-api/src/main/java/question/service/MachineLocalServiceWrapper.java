/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package question.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MachineLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see MachineLocalService
 * @generated
 */
public class MachineLocalServiceWrapper
	implements MachineLocalService, ServiceWrapper<MachineLocalService> {

	public MachineLocalServiceWrapper(MachineLocalService machineLocalService) {
		_machineLocalService = machineLocalService;
	}

	/**
	 * Adds the machine to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MachineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param machine the machine
	 * @return the machine that was added
	 */
	@Override
	public question.model.Machine addMachine(question.model.Machine machine) {
		return _machineLocalService.addMachine(machine);
	}

	/**
	 * Creates a new machine with the primary key. Does not add the machine to the database.
	 *
	 * @param machineId the primary key for the new machine
	 * @return the new machine
	 */
	@Override
	public question.model.Machine createMachine(long machineId) {
		return _machineLocalService.createMachine(machineId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _machineLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the machine with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MachineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param machineId the primary key of the machine
	 * @return the machine that was removed
	 * @throws PortalException if a machine with the primary key could not be found
	 */
	@Override
	public question.model.Machine deleteMachine(long machineId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _machineLocalService.deleteMachine(machineId);
	}

	/**
	 * Deletes the machine from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MachineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param machine the machine
	 * @return the machine that was removed
	 */
	@Override
	public question.model.Machine deleteMachine(
		question.model.Machine machine) {

		return _machineLocalService.deleteMachine(machine);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _machineLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _machineLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _machineLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>question.model.impl.MachineModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _machineLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>question.model.impl.MachineModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _machineLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _machineLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _machineLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public question.model.Machine fetchMachine(long machineId) {
		return _machineLocalService.fetchMachine(machineId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _machineLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _machineLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the machine with the primary key.
	 *
	 * @param machineId the primary key of the machine
	 * @return the machine
	 * @throws PortalException if a machine with the primary key could not be found
	 */
	@Override
	public question.model.Machine getMachine(long machineId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _machineLocalService.getMachine(machineId);
	}

	@Override
	public java.util.List<question.model.Machine> getMachine(
		long companyId, int start, int end) {

		return _machineLocalService.getMachine(companyId, start, end);
	}

	@Override
	public int getMachineCount(long companyId) {
		return _machineLocalService.getMachineCount(companyId);
	}

	/**
	 * Returns a range of all the machines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>question.model.impl.MachineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @return the range of machines
	 */
	@Override
	public java.util.List<question.model.Machine> getMachines(
		int start, int end) {

		return _machineLocalService.getMachines(start, end);
	}

	/**
	 * Returns the number of machines.
	 *
	 * @return the number of machines
	 */
	@Override
	public int getMachinesCount() {
		return _machineLocalService.getMachinesCount();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _machineLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _machineLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the machine in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MachineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param machine the machine
	 * @return the machine that was updated
	 */
	@Override
	public question.model.Machine updateMachine(
		question.model.Machine machine) {

		return _machineLocalService.updateMachine(machine);
	}

	@Override
	public MachineLocalService getWrappedService() {
		return _machineLocalService;
	}

	@Override
	public void setWrappedService(MachineLocalService machineLocalService) {
		_machineLocalService = machineLocalService;
	}

	private MachineLocalService _machineLocalService;

}