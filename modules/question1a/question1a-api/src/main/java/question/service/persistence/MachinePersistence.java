/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package question.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

import question.exception.NoSuchMachineException;

import question.model.Machine;

/**
 * The persistence interface for the machine service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MachineUtil
 * @generated
 */
@ProviderType
public interface MachinePersistence extends BasePersistence<Machine> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MachineUtil} to access the machine persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the machines where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching machines
	 */
	public java.util.List<Machine> findByCompanyId(long companyId);

	/**
	 * Returns a range of all the machines where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @return the range of matching machines
	 */
	public java.util.List<Machine> findByCompanyId(
		long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the machines where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching machines
	 */
	public java.util.List<Machine> findByCompanyId(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Machine>
			orderByComparator);

	/**
	 * Returns an ordered range of all the machines where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching machines
	 */
	public java.util.List<Machine> findByCompanyId(
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Machine>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first machine in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching machine
	 * @throws NoSuchMachineException if a matching machine could not be found
	 */
	public Machine findByCompanyId_First(
			long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Machine>
				orderByComparator)
		throws NoSuchMachineException;

	/**
	 * Returns the first machine in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching machine, or <code>null</code> if a matching machine could not be found
	 */
	public Machine fetchByCompanyId_First(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Machine>
			orderByComparator);

	/**
	 * Returns the last machine in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching machine
	 * @throws NoSuchMachineException if a matching machine could not be found
	 */
	public Machine findByCompanyId_Last(
			long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Machine>
				orderByComparator)
		throws NoSuchMachineException;

	/**
	 * Returns the last machine in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching machine, or <code>null</code> if a matching machine could not be found
	 */
	public Machine fetchByCompanyId_Last(
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Machine>
			orderByComparator);

	/**
	 * Returns the machines before and after the current machine in the ordered set where companyId = &#63;.
	 *
	 * @param machineId the primary key of the current machine
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next machine
	 * @throws NoSuchMachineException if a machine with the primary key could not be found
	 */
	public Machine[] findByCompanyId_PrevAndNext(
			long machineId, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Machine>
				orderByComparator)
		throws NoSuchMachineException;

	/**
	 * Removes all the machines where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	public void removeByCompanyId(long companyId);

	/**
	 * Returns the number of machines where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching machines
	 */
	public int countByCompanyId(long companyId);

	/**
	 * Caches the machine in the entity cache if it is enabled.
	 *
	 * @param machine the machine
	 */
	public void cacheResult(Machine machine);

	/**
	 * Caches the machines in the entity cache if it is enabled.
	 *
	 * @param machines the machines
	 */
	public void cacheResult(java.util.List<Machine> machines);

	/**
	 * Creates a new machine with the primary key. Does not add the machine to the database.
	 *
	 * @param machineId the primary key for the new machine
	 * @return the new machine
	 */
	public Machine create(long machineId);

	/**
	 * Removes the machine with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param machineId the primary key of the machine
	 * @return the machine that was removed
	 * @throws NoSuchMachineException if a machine with the primary key could not be found
	 */
	public Machine remove(long machineId) throws NoSuchMachineException;

	public Machine updateImpl(Machine machine);

	/**
	 * Returns the machine with the primary key or throws a <code>NoSuchMachineException</code> if it could not be found.
	 *
	 * @param machineId the primary key of the machine
	 * @return the machine
	 * @throws NoSuchMachineException if a machine with the primary key could not be found
	 */
	public Machine findByPrimaryKey(long machineId)
		throws NoSuchMachineException;

	/**
	 * Returns the machine with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param machineId the primary key of the machine
	 * @return the machine, or <code>null</code> if a machine with the primary key could not be found
	 */
	public Machine fetchByPrimaryKey(long machineId);

	/**
	 * Returns all the machines.
	 *
	 * @return the machines
	 */
	public java.util.List<Machine> findAll();

	/**
	 * Returns a range of all the machines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @return the range of machines
	 */
	public java.util.List<Machine> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the machines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of machines
	 */
	public java.util.List<Machine> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Machine>
			orderByComparator);

	/**
	 * Returns an ordered range of all the machines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of machines
	 */
	public java.util.List<Machine> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Machine>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the machines from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of machines.
	 *
	 * @return the number of machines
	 */
	public int countAll();

}