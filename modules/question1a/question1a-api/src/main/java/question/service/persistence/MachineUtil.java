/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package question.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

import question.model.Machine;

/**
 * The persistence utility for the machine service. This utility wraps <code>question.service.persistence.impl.MachinePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MachinePersistence
 * @generated
 */
public class MachineUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Machine machine) {
		getPersistence().clearCache(machine);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Machine> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Machine> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Machine> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Machine> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Machine> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Machine update(Machine machine) {
		return getPersistence().update(machine);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Machine update(
		Machine machine, ServiceContext serviceContext) {

		return getPersistence().update(machine, serviceContext);
	}

	/**
	 * Returns all the machines where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching machines
	 */
	public static List<Machine> findByCompanyId(long companyId) {
		return getPersistence().findByCompanyId(companyId);
	}

	/**
	 * Returns a range of all the machines where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @return the range of matching machines
	 */
	public static List<Machine> findByCompanyId(
		long companyId, int start, int end) {

		return getPersistence().findByCompanyId(companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the machines where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching machines
	 */
	public static List<Machine> findByCompanyId(
		long companyId, int start, int end,
		OrderByComparator<Machine> orderByComparator) {

		return getPersistence().findByCompanyId(
			companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the machines where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching machines
	 */
	public static List<Machine> findByCompanyId(
		long companyId, int start, int end,
		OrderByComparator<Machine> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByCompanyId(
			companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first machine in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching machine
	 * @throws NoSuchMachineException if a matching machine could not be found
	 */
	public static Machine findByCompanyId_First(
			long companyId, OrderByComparator<Machine> orderByComparator)
		throws question.exception.NoSuchMachineException {

		return getPersistence().findByCompanyId_First(
			companyId, orderByComparator);
	}

	/**
	 * Returns the first machine in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching machine, or <code>null</code> if a matching machine could not be found
	 */
	public static Machine fetchByCompanyId_First(
		long companyId, OrderByComparator<Machine> orderByComparator) {

		return getPersistence().fetchByCompanyId_First(
			companyId, orderByComparator);
	}

	/**
	 * Returns the last machine in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching machine
	 * @throws NoSuchMachineException if a matching machine could not be found
	 */
	public static Machine findByCompanyId_Last(
			long companyId, OrderByComparator<Machine> orderByComparator)
		throws question.exception.NoSuchMachineException {

		return getPersistence().findByCompanyId_Last(
			companyId, orderByComparator);
	}

	/**
	 * Returns the last machine in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching machine, or <code>null</code> if a matching machine could not be found
	 */
	public static Machine fetchByCompanyId_Last(
		long companyId, OrderByComparator<Machine> orderByComparator) {

		return getPersistence().fetchByCompanyId_Last(
			companyId, orderByComparator);
	}

	/**
	 * Returns the machines before and after the current machine in the ordered set where companyId = &#63;.
	 *
	 * @param machineId the primary key of the current machine
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next machine
	 * @throws NoSuchMachineException if a machine with the primary key could not be found
	 */
	public static Machine[] findByCompanyId_PrevAndNext(
			long machineId, long companyId,
			OrderByComparator<Machine> orderByComparator)
		throws question.exception.NoSuchMachineException {

		return getPersistence().findByCompanyId_PrevAndNext(
			machineId, companyId, orderByComparator);
	}

	/**
	 * Removes all the machines where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	public static void removeByCompanyId(long companyId) {
		getPersistence().removeByCompanyId(companyId);
	}

	/**
	 * Returns the number of machines where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching machines
	 */
	public static int countByCompanyId(long companyId) {
		return getPersistence().countByCompanyId(companyId);
	}

	/**
	 * Caches the machine in the entity cache if it is enabled.
	 *
	 * @param machine the machine
	 */
	public static void cacheResult(Machine machine) {
		getPersistence().cacheResult(machine);
	}

	/**
	 * Caches the machines in the entity cache if it is enabled.
	 *
	 * @param machines the machines
	 */
	public static void cacheResult(List<Machine> machines) {
		getPersistence().cacheResult(machines);
	}

	/**
	 * Creates a new machine with the primary key. Does not add the machine to the database.
	 *
	 * @param machineId the primary key for the new machine
	 * @return the new machine
	 */
	public static Machine create(long machineId) {
		return getPersistence().create(machineId);
	}

	/**
	 * Removes the machine with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param machineId the primary key of the machine
	 * @return the machine that was removed
	 * @throws NoSuchMachineException if a machine with the primary key could not be found
	 */
	public static Machine remove(long machineId)
		throws question.exception.NoSuchMachineException {

		return getPersistence().remove(machineId);
	}

	public static Machine updateImpl(Machine machine) {
		return getPersistence().updateImpl(machine);
	}

	/**
	 * Returns the machine with the primary key or throws a <code>NoSuchMachineException</code> if it could not be found.
	 *
	 * @param machineId the primary key of the machine
	 * @return the machine
	 * @throws NoSuchMachineException if a machine with the primary key could not be found
	 */
	public static Machine findByPrimaryKey(long machineId)
		throws question.exception.NoSuchMachineException {

		return getPersistence().findByPrimaryKey(machineId);
	}

	/**
	 * Returns the machine with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param machineId the primary key of the machine
	 * @return the machine, or <code>null</code> if a machine with the primary key could not be found
	 */
	public static Machine fetchByPrimaryKey(long machineId) {
		return getPersistence().fetchByPrimaryKey(machineId);
	}

	/**
	 * Returns all the machines.
	 *
	 * @return the machines
	 */
	public static List<Machine> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the machines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @return the range of machines
	 */
	public static List<Machine> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the machines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of machines
	 */
	public static List<Machine> findAll(
		int start, int end, OrderByComparator<Machine> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the machines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MachineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of machines
	 * @param end the upper bound of the range of machines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of machines
	 */
	public static List<Machine> findAll(
		int start, int end, OrderByComparator<Machine> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the machines from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of machines.
	 *
	 * @return the number of machines
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static MachinePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<MachinePersistence, MachinePersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(MachinePersistence.class);

		ServiceTracker<MachinePersistence, MachinePersistence> serviceTracker =
			new ServiceTracker<MachinePersistence, MachinePersistence>(
				bundle.getBundleContext(), MachinePersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}