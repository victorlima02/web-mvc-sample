/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package question.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import question.model.Access;

/**
 * The cache model class for representing Access in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class AccessCacheModel implements CacheModel<Access>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AccessCacheModel)) {
			return false;
		}

		AccessCacheModel accessCacheModel = (AccessCacheModel)object;

		if (accessId == accessCacheModel.accessId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, accessId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", accessId=");
		sb.append(accessId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", accessDate=");
		sb.append(accessDate);
		sb.append(", machineId=");
		sb.append(machineId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Access toEntityModel() {
		AccessImpl accessImpl = new AccessImpl();

		if (uuid == null) {
			accessImpl.setUuid("");
		}
		else {
			accessImpl.setUuid(uuid);
		}

		accessImpl.setAccessId(accessId);
		accessImpl.setCompanyId(companyId);
		accessImpl.setGroupId(groupId);

		if (userName == null) {
			accessImpl.setUserName("");
		}
		else {
			accessImpl.setUserName(userName);
		}

		if (accessDate == Long.MIN_VALUE) {
			accessImpl.setAccessDate(null);
		}
		else {
			accessImpl.setAccessDate(new Date(accessDate));
		}

		accessImpl.setMachineId(machineId);
		accessImpl.setUserId(userId);

		accessImpl.resetOriginalValues();

		return accessImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		accessId = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();
		userName = objectInput.readUTF();
		accessDate = objectInput.readLong();

		machineId = objectInput.readLong();

		userId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(accessId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(accessDate);

		objectOutput.writeLong(machineId);

		objectOutput.writeLong(userId);
	}

	public String uuid;
	public long accessId;
	public long companyId;
	public long groupId;
	public String userName;
	public long accessDate;
	public long machineId;
	public long userId;

}