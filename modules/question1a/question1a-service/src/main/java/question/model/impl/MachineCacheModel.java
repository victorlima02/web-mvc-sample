/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package question.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import question.model.Machine;

/**
 * The cache model class for representing Machine in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class MachineCacheModel implements CacheModel<Machine>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof MachineCacheModel)) {
			return false;
		}

		MachineCacheModel machineCacheModel = (MachineCacheModel)object;

		if (machineId == machineCacheModel.machineId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, machineId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{machineId=");
		sb.append(machineId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", adminId=");
		sb.append(adminId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Machine toEntityModel() {
		MachineImpl machineImpl = new MachineImpl();

		machineImpl.setMachineId(machineId);
		machineImpl.setCompanyId(companyId);
		machineImpl.setGroupId(groupId);
		machineImpl.setAdminId(adminId);

		machineImpl.resetOriginalValues();

		return machineImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		machineId = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();

		adminId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(machineId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(adminId);
	}

	public long machineId;
	public long companyId;
	public long groupId;
	public long adminId;

}