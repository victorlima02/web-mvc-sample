/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package question.service.impl;

import com.liferay.portal.aop.AopService;

import org.osgi.service.component.annotations.Component;

import question.model.*;
import question.service.base.AccessLocalServiceBaseImpl;

import java.util.List;

/**
 * The implementation of the access local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>question.service.AccessLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AccessLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=question.model.Access",
	service = AopService.class
)
public class AccessLocalServiceImpl extends AccessLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>question.service.AccessLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>question.service.AccessLocalServiceUtil</code>.
	 */

	public List< Access > getAccesses( ){

		return accessPersistence.findAll( );
	}

	@Override
	public Access addAccess( Access access ) {

		if ( !access.isNew( ) ) {
			return updateAccess( access );
		}

		access.setPrimaryKey( counterLocalService.increment( Access.class.getName( ) ) );

		return super.addAccess( access );
	}
	
	public List< Access > getAccesses( long companyId, int start, int end){

		return accessPersistence.findByCompanyId( companyId, start, end );
	}

	public int getAccessesCount( long companyId){

		return accessPersistence.countByCompanyId( companyId);
	}
}
