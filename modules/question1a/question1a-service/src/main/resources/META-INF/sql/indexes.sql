create index IX_3BB28509 on Question_Access (accessDate);
create index IX_F95DEBE5 on Question_Access (companyId);
create index IX_AAAFEAF on Question_Access (machineId);
create index IX_70C7D19D on Question_Access (userId);
create index IX_2FF2DB97 on Question_Access (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_B2DB5BD9 on Question_Access (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_1C23E74 on Question_Machine (companyId);