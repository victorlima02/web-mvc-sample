create table Question_Access (
	uuid_ VARCHAR(75) null,
	accessId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userName VARCHAR(75) null,
	accessDate DATE null,
	machineId LONG,
	userId LONG
);

create table Question_Machine (
	machineId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	adminId LONG
);