package access.access;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import org.osgi.service.component.annotations.*;
import question.model.Access;
import question.service.AccessLocalService;

import javax.portlet.*;
import java.util.Date;

import static access.constants.AccessPortletKeys.*;

@Component( immediate = true,
			property = {
					"javax.portlet.name=" + ACCESS,
					"mvc.command.name=" + ACCESS_ADD
			},
			service = MVCActionCommand.class )
public class AddAccessAction implements MVCActionCommand {

	@Reference
	private volatile AccessLocalService accessLocalServiceService;

	@Override
	public boolean processAction( ActionRequest request, ActionResponse response ) {

		AddAccessActionOptions options = new AddAccessActionOptions( request );

		if ( options.isValid( ) ) {

			Access access = createAccess( options );

			access.persist( );

			return true;

		}

		return false;
	}

	private Access createAccess( AddAccessActionOptions options ) {

		Access newAccess = accessLocalServiceService.createAccess( 0 );

		newAccess.setCompanyId( options.getCompanyId( ) );
		newAccess.setGroupId( options.getGroupId( ) );

		newAccess.setUserId( options.getUserId( ) );
		newAccess.setMachineId( options.getMachineId( ) );
		newAccess.setAccessDate( new Date( ) );

		return newAccess;
	}
}
