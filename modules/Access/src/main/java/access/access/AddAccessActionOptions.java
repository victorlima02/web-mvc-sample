package access.access;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;

import javax.portlet.ActionRequest;

import static com.liferay.portal.kernel.util.WebKeys.THEME_DISPLAY;
import static java.lang.Long.parseLong;

public class AddAccessActionOptions {

	private boolean valid = true;
	private long userId;
	private long machineId;
	private final long groupId;
	private final long companyId;

	public AddAccessActionOptions( ActionRequest request ) {

		ThemeDisplay themeDisplay = ( ThemeDisplay ) request.getAttribute( THEME_DISPLAY );

		groupId = themeDisplay.getSiteGroupId( );
		companyId = themeDisplay.getCompanyId( );

		setUserId( request );
		setMachineId( request );
	}

	public long getCompanyId( ) {

		return companyId;
	}

	public long getGroupId( ) {

		return groupId;
	}

	public long getMachineId( ) {

		return machineId;
	}

	private void setMachineId( ActionRequest request ) {

		String raw = request.getParameter( "machine-id" );
		Long parameter = Validator.isNumber( raw )
						 ? parseLong( raw.trim( ) )
						 : null;

		if ( parameter != null ) {
			this.machineId = parameter;
		}
		else {
			SessionErrors.add( request, "machine-id-not-valid" );
			valid = false;
		}
	}

	public long getUserId( ) {

		return userId;
	}

	private void setUserId( ActionRequest request ) {

		String raw = request.getParameter( "user-id" );
		Long parameter = Validator.isNumber( raw )
						 ? parseLong( raw.trim( ) )
						 : null;

		if ( parameter != null ) {
			this.userId = parameter;
		}
		else {
			SessionErrors.add( request, "user-id-not-valid" );
			valid = false;
		}
	}

	public boolean isValid( ) {

		return valid;
	}
}
