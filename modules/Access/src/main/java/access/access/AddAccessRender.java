package access.access;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import org.osgi.service.component.annotations.*;
import question.service.MachineLocalService;

import javax.portlet.*;

import static access.constants.AccessPortletKeys.*;

@Component( immediate = true,
			property = {
					"javax.portlet.name=" + ACCESS,
					"mvc.command.name=" + ACCESS_ADD
			},
			service = MVCRenderCommand.class )
public class AddAccessRender implements MVCRenderCommand {

	@Reference
	private volatile UserLocalService userLocalService;
	@Reference
	private volatile MachineLocalService machineLocalService;

	@Override
	public String render( RenderRequest request, RenderResponse response ) {

		request.setAttribute( "userLocalService", userLocalService );
		request.setAttribute( "machineLocalService", machineLocalService );

		return "/add_access.jsp";
	}
}
