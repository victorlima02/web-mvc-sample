package access.constants;

public class AccessPortletKeys {

	public static final String ACCESS = "access_AccessPortlet";
	public static final String ACCESS_ADD = "/access/add";
	public static final String MACHINE_RENDER = "/machine/view";
	public static final String MACHINE_ADD = "/machine/add";

}
