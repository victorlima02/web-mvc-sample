package access.portlet;

import access.constants.AccessPortletKeys;
import access.search.AccessSearchUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import org.osgi.service.component.annotations.*;

import javax.portlet.*;
import java.io.IOException;

@Component( immediate = true,
			property = {
					"com.liferay.portlet.display-category=category.sample",
					"com.liferay.portlet.header-portlet-css=/css/main.css",
					"com.liferay.portlet.instanceable=true",
					"javax.portlet.display-name=Access",
					"javax.portlet.init-param.template-path=/",
					"javax.portlet.init-param.view-template=/view.jsp",
					"javax.portlet.name=" + AccessPortletKeys.ACCESS,
					"javax.portlet.resource-bundle=content.Language",
					"javax.portlet.security-role-ref=power-user,user"
			},
			service = Portlet.class )
public class AccessPortlet extends MVCPortlet {

	@Reference
	private volatile AccessSearchUtil accessSearchUtil;

	@Override
	public void render( RenderRequest renderRequest, RenderResponse renderResponse ) throws PortletException, IOException {

		renderRequest.setAttribute( "accessSearchUtil", accessSearchUtil );
		setNavigationDisplayContext( renderRequest, renderResponse );
		setToolbarDisplayContext( renderRequest, renderResponse );
		super.render( renderRequest, renderResponse );
	}

	private void setNavigationDisplayContext( RenderRequest request, RenderResponse response ) {

		NavigationDisplayContext navigationDisplayContext = new NavigationDisplayContext( request, response );
		request.setAttribute( "navigationDisplayContext", navigationDisplayContext );
	}

	private void setToolbarDisplayContext( RenderRequest request, RenderResponse response ) {

		AccessLogToolbarDisplayContext accessLogToolbarDisplayContext = new AccessLogToolbarDisplayContext( request, response );
		request.setAttribute( "accessLogToolbarDisplayContext", accessLogToolbarDisplayContext );
	}

}
