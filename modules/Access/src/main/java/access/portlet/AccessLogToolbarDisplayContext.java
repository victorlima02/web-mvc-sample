package access.portlet;

import com.liferay.frontend.taglib.clay.servlet.taglib.util.CreationMenu;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import javax.portlet.*;
import javax.servlet.http.HttpServletRequest;

import static access.constants.AccessPortletKeys.*;
import static com.liferay.portal.kernel.util.PortalUtil.getHttpServletRequest;

public class AccessLogToolbarDisplayContext {

	private final String currentURL;
	private final RenderResponse renderResponse;
	private final RenderRequest request;

	public AccessLogToolbarDisplayContext( RenderRequest request, RenderResponse renderResponse ) {

		this.request = request;
		this.renderResponse = renderResponse;
		currentURL = getCurrentCompleteURL( request );
	}

	private String getCurrentCompleteURL( PortletRequest request ) {

		return PortalUtil.getCurrentCompleteURL( PortalUtil.getHttpServletRequest( request ) );
	}

	public CreationMenu getCreationMenu( ) {

		CreationMenu menu = new CreationMenu( );
		HttpServletRequest servletRequest = getHttpServletRequest( request );

		menu.addPrimaryDropdownItem( dropdownItem -> {
			dropdownItem.setHref( renderResponse.createRenderURL( ), "mvcRenderCommandName", MACHINE_ADD, "backUrl", currentURL );
			dropdownItem.setLabel( LanguageUtil.get( servletRequest, "add-machine" ) );
		} );

		menu.addPrimaryDropdownItem( dropdownItem -> {
			dropdownItem.setHref( renderResponse.createRenderURL( ), "mvcRenderCommandName", ACCESS_ADD, "backUrl", currentURL );
			dropdownItem.setLabel( LanguageUtil.get( servletRequest, "add-access" ) );
		} );

		return menu;
	}

}
