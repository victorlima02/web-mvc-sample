package access.portlet;

import com.liferay.frontend.taglib.clay.servlet.taglib.util.*;
import com.liferay.portal.kernel.language.LanguageUtil;

import javax.portlet.*;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static access.constants.AccessPortletKeys.MACHINE_RENDER;
import static com.liferay.portal.kernel.util.PortalUtil.getHttpServletRequest;

public class NavigationDisplayContext {

	private final RenderRequest request;
	private final RenderResponse response;

	public NavigationDisplayContext( RenderRequest request, RenderResponse response ) {

		this.request = request;
		this.response = response;
	}

	public List< NavigationItem > getNavigationItems( String key ) {

		NavigationItemList menu = new NavigationItemList( );
		HttpServletRequest servletRequest = getHttpServletRequest( request );

		PortletURL machinesUrl = response.createRenderURL( );
		machinesUrl.setParameter( "mvcRenderCommandName", MACHINE_RENDER );

		PortletURL logUrl = response.createRenderURL( );

		menu.add( navigationItem -> {
			navigationItem.setActive( "log".equals( key ) );
			navigationItem.setHref( logUrl );
			navigationItem.setLabel( LanguageUtil.get( servletRequest, "log" ) );
		} );

		menu.add( navigationItem -> {
			navigationItem.setActive( "machines".equals( key ) );
			navigationItem.setHref( machinesUrl );
			navigationItem.setLabel( LanguageUtil.get( servletRequest, "machines" ) );
		} );

		return menu;
	}
}
