package access.search;

import org.osgi.service.component.annotations.*;
import question.model.Machine;
import question.service.MachineLocalService;

import java.util.List;

@Component( immediate = true,
			service = MachineSearchUtil.class )
public class MachineSearchUtil {

	@Reference
	private volatile MachineLocalService machineLocalService;

	public List< Machine > fetchMachine( long companyId, int start, int end ) {

		List< Machine > machine = machineLocalService.getMachine( companyId, start, end );

		return machine;
	}

	public Integer fetchMachineCount( long companyId ) {

		int count = machineLocalService.getMachineCount( companyId );
		return count;
	}
}
