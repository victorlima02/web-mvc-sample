package access.search;

import org.osgi.service.component.annotations.*;
import question.model.Access;
import question.service.AccessLocalService;

import java.util.List;

@Component( immediate = true,
			service = AccessSearchUtil.class )
public class AccessSearchUtil {

	@Reference
	private volatile AccessLocalService accessLocalService;

	public List< Access > fetchAccess( long companyId, int start, int end ) {

		List< Access > access = accessLocalService.getAccesses( companyId, start, end );

		return access;
	}

	public Integer fetchAccessCount( long companyId ) {

		int count = accessLocalService.getAccessesCount( companyId );
		return count;
	}
}
