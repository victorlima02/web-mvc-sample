package access.machine;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;

import javax.portlet.ActionRequest;

import static com.liferay.portal.kernel.util.WebKeys.THEME_DISPLAY;
import static java.lang.Long.parseLong;

public class AddMachineActionOptions {

	private Long adminId;
	private boolean valid = true;
	private final long groupId;
	private final long companyId;

	public AddMachineActionOptions( ActionRequest request ) {

		ThemeDisplay themeDisplay = ( ThemeDisplay ) request.getAttribute( THEME_DISPLAY );

		groupId = themeDisplay.getSiteGroupId( );
		companyId = themeDisplay.getCompanyId( );

		setAdminId( request );
	}

	public Long getAdminId( ) {

		return adminId;
	}

	private void setAdminId( ActionRequest request ) {

		String raw = request.getParameter( "admin-id" );
		Long parameter = Validator.isNumber( raw )
						 ? parseLong( raw.trim( ) )
						 : null;

		if ( parameter != null ) {
			this.adminId = parameter;
		}
		else {
			SessionErrors.add( request, "admin-id-not-valid" );
			valid = false;
		}

	}

	public long getCompanyId( ) {

		return companyId;
	}

	public long getGroupId( ) {

		return groupId;
	}

	public boolean isValid( ) {

		return valid;
	}
}
