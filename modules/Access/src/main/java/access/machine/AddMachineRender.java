package access.machine;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import org.osgi.service.component.annotations.*;

import javax.portlet.*;

import static access.constants.AccessPortletKeys.*;

@Component( immediate = true,
			property = {
					"javax.portlet.name=" + ACCESS,
					"mvc.command.name=" + MACHINE_ADD
			},
			service = MVCRenderCommand.class )
public class AddMachineRender implements MVCRenderCommand {

	@Reference
	private volatile UserLocalService userLocalService;

	@Override
	public String render( RenderRequest request, RenderResponse response ) {

		request.setAttribute( "userLocalService", userLocalService );

		return "/machine/add_machine.jsp";
	}
}
