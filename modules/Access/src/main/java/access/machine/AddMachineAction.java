package access.machine;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import org.osgi.service.component.annotations.*;
import question.model.Machine;
import question.service.MachineLocalService;

import javax.portlet.*;

import static access.constants.AccessPortletKeys.*;

@Component( immediate = true,
			property = {
					"javax.portlet.name=" + ACCESS,
					"mvc.command.name=" + MACHINE_ADD
			},
			service = MVCActionCommand.class )
public class AddMachineAction implements MVCActionCommand {

	@Reference
	private volatile MachineLocalService machineService;

	@Override
	public boolean processAction( ActionRequest request, ActionResponse response ) {

		AddMachineActionOptions options = new AddMachineActionOptions( request );

		if ( options.isValid( ) ) {

			Machine machine = createMachine( options );

			machine.persist( );

			return true;

		}

		return false;
	}

	private Machine createMachine( AddMachineActionOptions options ) {

		Machine newMachine = machineService.createMachine( 0 );

		newMachine.setAdminId( options.getAdminId( ) );
		newMachine.setCompanyId( options.getCompanyId( ) );
		newMachine.setGroupId( options.getGroupId( ) );

		return newMachine;
	}
}
