package access.machine;

import access.constants.AccessPortletKeys;
import access.search.MachineSearchUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import org.osgi.service.component.annotations.*;

import javax.portlet.*;

import static access.constants.AccessPortletKeys.MACHINE_RENDER;

@Component( immediate = true,
			property = {
					"javax.portlet.name=" + AccessPortletKeys.ACCESS,
					"mvc.command.name=" + MACHINE_RENDER
			},
			service = MVCRenderCommand.class )
public class ViewMachinesRender implements MVCRenderCommand {

	@Reference
	private volatile MachineSearchUtil machineSearchUtil;

	@Override
	public String render( RenderRequest request, RenderResponse response ) {

		request.setAttribute( "machineSearchUtil", machineSearchUtil );
		setToolbarDisplayContext( request, response );
		return "/machine/view_machines.jsp";
	}

	private void setToolbarDisplayContext( RenderRequest request, RenderResponse response ) {

		ViewMachineToolbarDisplayContext machineToolbarDisplayContext = new ViewMachineToolbarDisplayContext( request, response );
		request.setAttribute( "machineToolbarDisplayContext", machineToolbarDisplayContext );
	}
}
