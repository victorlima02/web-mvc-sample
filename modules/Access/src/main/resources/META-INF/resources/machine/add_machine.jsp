<%@ include file="../init.jsp" %>

<jsp:useBean id='userLocalService' scope='request' type='com.liferay.portal.kernel.service.UserLocalService'/>
<c:set var="userCount" value="${userLocalService.getCompanyUsersCount(companyId)}"/>

<portlet:actionURL var='addMachine' name='/machine/add'/>

<liferay-ui:header backURL="${param.backUrl}" title="back"/>

<aui:form action="${addMachine}" method="post">
	<aui:fieldset>
		<aui:select label="admin" name="admin-id" id="admin" required="true" showEmptyOption="true">
			<c:forEach items="${userLocalService.getCompanyUsers(companyId,0,userCount)}" var="user">
				<aui:option label="${user.fullName}" value="${user.userId}"/>
			</c:forEach>
		</aui:select>
	</aui:fieldset>

	<aui:button-row>
		<aui:button type="submit"/>
	</aui:button-row>

</aui:form>

