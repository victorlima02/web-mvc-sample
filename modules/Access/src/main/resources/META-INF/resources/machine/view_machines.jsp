<%@ include file="../init.jsp" %>

<jsp:useBean id='machineSearchUtil' scope='request' type='access.search.MachineSearchUtil'/>
<jsp:useBean id='machineToolbarDisplayContext' scope='request' type='access.machine.ViewMachineToolbarDisplayContext'/>

<clay:navigation-bar
		inverted='true'
		navigationItems='${navigationDisplayContext.getNavigationItems("machines")}'
/>

<clay:management-toolbar
		creationMenu='${machineToolbarDisplayContext.creationMenu}'
		searchContainerId='machineLog'
		showCreationMenu='true'
		showSearch='false'
		selectable='false'
/>

<liferay-ui:search-container emptyResultsMessage="there-are-no-machines" delta="7" id='machineLog'>

	<liferay-ui:search-container-results>
		<%
			results = machineSearchUtil.fetchMachine( company.getCompanyId( ), searchContainer.getStart( ), searchContainer.getEnd( ) );
			total = machineSearchUtil.fetchMachineCount( company.getCompanyId( ) );
			pageContext.setAttribute( "results", results );
			pageContext.setAttribute( "total", total );
		%>
	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row className="question.model.Machine" keyProperty="primaryKey" modelVar="machine">

		<liferay-ui:search-container-column-text name="machine-id" property="machineId"/>

		<liferay-ui:search-container-column-text name="admin-id" property="adminId"/>

		<liferay-ui:search-container-column-user name="admin" property="adminId"/>

	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator/>

</liferay-ui:search-container>
