<%@ include file="init.jsp" %>

<jsp:useBean id='userLocalService' scope='request' type='com.liferay.portal.kernel.service.UserLocalService'/>
<c:set var="userCount" value="${userLocalService.getCompanyUsersCount(companyId)}"/>

<jsp:useBean id='machineLocalService' scope='request' type='question.service.MachineLocalService'/>
<c:set var="machineCount" value="${machineLocalService.getMachineCount(companyId)}"/>

<portlet:actionURL var='addAccess' name='/access/add'/>

<liferay-ui:header backURL="${param.backUrl}" title="back"/>

<aui:form action="${addAccess}" method="post">
	<aui:fieldset>
		<aui:select label="machine" name="machine-id" id="machine" required="true" showEmptyOption="true">
			<c:forEach items="${machineLocalService.getMachine(companyId,0,machineCount)}" var="machine">
				<aui:option label="${machine.machineId}" value="${machine.machineId}"/>
			</c:forEach>
		</aui:select>

		<aui:select label="user" name="user-id" id="user" required="true" showEmptyOption="true">
			<c:forEach items="${userLocalService.getCompanyUsers(companyId,0,userCount)}" var="user">
				<aui:option label="${user.fullName}" value="${user.userId}"/>
			</c:forEach>
		</aui:select>
	</aui:fieldset>

	<aui:button-row>
		<aui:button type="submit"/>
	</aui:button-row>

</aui:form>

