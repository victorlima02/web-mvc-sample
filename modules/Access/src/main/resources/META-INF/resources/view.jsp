<%@ include file="/init.jsp" %>

<jsp:useBean id='accessSearchUtil' scope='request' type='access.search.AccessSearchUtil'/>
<jsp:useBean id='navigationDisplayContext' scope='request' type='access.portlet.NavigationDisplayContext'/>
<jsp:useBean id='accessLogToolbarDisplayContext' scope='request' type='access.portlet.AccessLogToolbarDisplayContext'/>

<portlet:renderURL var='viewURL'/>

<portlet:renderURL var='addAccess'>
	<portlet:param name='mvcRenderCommandName' value='/access/add'/>
	<portlet:param name='backUrl' value='${viewURL}'/>
</portlet:renderURL>

<clay:navigation-bar
		inverted='true'
		navigationItems='${navigationDisplayContext.getNavigationItems("log")}'
/>

<clay:management-toolbar
		creationMenu='${accessLogToolbarDisplayContext.creationMenu}'
		searchContainerId='accessLog'
		showCreationMenu='true'
		showSearch='false'
		selectable='false'
/>

<liferay-ui:search-container emptyResultsMessage="there-are-no-logged-access" delta="7" id='accessLog'>

	<liferay-ui:search-container-results>
		<%
			results = accessSearchUtil.fetchAccess( company.getCompanyId( ), searchContainer.getStart( ), searchContainer.getEnd( ) );
			total = accessSearchUtil.fetchAccessCount( company.getCompanyId( ) );
			pageContext.setAttribute( "results", results );
			pageContext.setAttribute( "total", total );
		%>
	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row className="question.model.Access" keyProperty="primaryKey" modelVar="access">

		<liferay-ui:search-container-column-text name="machine-id" property="machineId"/>

		<liferay-ui:search-container-column-user name="user" property="userId"/>

		<liferay-ui:search-container-column-date name="access-date" property="accessDate"/>

	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator/>

</liferay-ui:search-container>
